package com.sdokara.samples.ssod;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import org.joda.time.DateTime;
import org.springframework.stereotype.Component;

import java.security.*;
import java.util.UUID;

@Component
public class JwtTokener {
    // these are normally kept in keystore file or somewhere, but i don't have the energy for it
    // so im gonna generate then at runtime and keep in memory
    private static PrivateKey privateKey;
    private static PublicKey publicKey;
    private static JwtParser parser;
    static {
        try {
            KeyPair keyPair = KeyPairGenerator.getInstance("RSA").generateKeyPair();
            privateKey = keyPair.getPrivate();
            publicKey = keyPair.getPublic();
        } catch (NoSuchAlgorithmException ignored) {
            // RSA exists in the sun packages
        }
        parser = Jwts.parserBuilder()
                .setSigningKey(publicKey)
                .build();
    }


    public String generate(User user) {
        // you can add as many claims as you want (they end up as json properties in the token)
        return Jwts.builder()
                .setId(UUID.randomUUID().toString())
                .setIssuer("http://my.service.com")
                .setAudience("my-service")
                .setIssuedAt(DateTime.now().toDate())
                .setExpiration(DateTime.now().plusMinutes(60).toDate())
                .setSubject(user.getId())
                .signWith(privateKey)
                .compact();
    }

    public String verify(String token) {
        // method returns the subject ends up being the id of the user only
        Jws<Claims> claims = parser.parseClaimsJws(token);
        return claims.getBody().getSubject();
    }
}
