package com.sdokara.samples.ssod;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;

@Slf4j
@Component
public class TokenAuthenticationFilter extends OncePerRequestFilter {
    private static final boolean isDebugEnabled = LoggerFactory.getLogger("org.springframework.security")
            .isDebugEnabled();

    private static final String BEARER = "bearer";
    // set to true if you have multiple authentication methods enabled
    private static final boolean ignoreFailure = false;

    private final JwtTokener tokener;
    private final UserRepository userRepository;

    public TokenAuthenticationFilter(JwtTokener tokener, UserRepository userRepository) {
        this.tokener = tokener;
        this.userRepository = userRepository;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        String token = extractHeaderToken(request);
        if (token == null) {
            filterChain.doFilter(request, response);
            return;
        }
        if (isDebugEnabled) {
            log.debug("Found compatible Authorization header");
        }
        String principal = null;
        try {
            principal = tokener.verify(token);
        } catch (Exception e) {
            if (isDebugEnabled) {
                log.debug("Token verification failed", e);
            }
        }
        if (principal != null) {
            User user = userRepository.findById(principal).orElse(null);
            if (user != null) {
                if (isDebugEnabled) {
                    log.debug("Authentication success for {}", principal);
                }
                SecurityContextHolder.getContext().setAuthentication(new TokenAuthentication(user, token));
                filterChain.doFilter(request, response);
                return;
            }
        }
        if (ignoreFailure) {
            if (isDebugEnabled) {
                log.debug("Authentication failed, ignoring");
            }
            filterChain.doFilter(request, response);
        } else {
            if (isDebugEnabled) {
                log.debug("Authentication failed");
            }
            response.addHeader(HttpHeaders.WWW_AUTHENTICATE, "Bearer");
            response.sendError(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED.getReasonPhrase());
        }
    }


    private static String extractHeaderToken(HttpServletRequest request) {
        Enumeration<String> headers = request.getHeaders(HttpHeaders.AUTHORIZATION);
        while (headers.hasMoreElements()) {
            String token = extractHeaderToken(headers.nextElement());
            if (token != null) {
                return token;
            }
        }
        return null;
    }

    private static String extractHeaderToken(String value) {
        if (value != null && value.toLowerCase().startsWith(BEARER)) {
            String authHeaderValue = value.substring(BEARER.length()).trim();
            int commaIndex = authHeaderValue.indexOf(',');
            if (commaIndex > 0) {
                authHeaderValue = authHeaderValue.substring(0, commaIndex);
            }
            return authHeaderValue;
        }
        return null;
    }
}