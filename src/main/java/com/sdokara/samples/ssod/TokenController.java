package com.sdokara.samples.ssod;

import lombok.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.annotation.security.PermitAll;
import java.util.Collections;
import java.util.Map;

@RestController
@RequestMapping("/tokens")
public class TokenController {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokener tokener;

    public TokenController(UserRepository userRepository, PasswordEncoder passwordEncoder, JwtTokener tokener) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.tokener = tokener;
    }

    // this is a pretty simplified flow - similar to oaut2 password grant, no refresh
    @PermitAll
    @PostMapping
    public @ResponseBody Map<String, Object> tokens(@RequestBody UsernamePassword body) {
        User user = userRepository.findByUsername(body.getUsername())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.UNAUTHORIZED));
        if (passwordEncoder.matches(body.getPassword(), user.getPassword())) {
            return Collections.singletonMap("token", tokener.generate(user));
        } else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
    }

    // lombok is a javac agent that generates code during compilation: constructors, accessors, equals, hashcode,
    // builders, etc.
    // install the lombok plugin and enjoy these annotations
    @Value
    public static class UsernamePassword {
        private String username;
        private String password;
    }
}
