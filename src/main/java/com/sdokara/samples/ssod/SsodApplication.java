package com.sdokara.samples.ssod;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SsodApplication {
    public static void main(String[] args) {
        SpringApplication.run(SsodApplication.class, args);
    }
}
