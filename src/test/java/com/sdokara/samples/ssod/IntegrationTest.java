package com.sdokara.samples.ssod;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.UUID;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = SsodApplication.class,
                webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
public class IntegrationTest {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @BeforeAll
    public static void setup(@LocalServerPort final int port,
                             @Autowired ServerProperties serverProperties) {
        RestAssured.port = port;
        RestAssured.basePath = serverProperties.getServlet().getContextPath();
    }

    @Test
    public void fullTest() { // naming lol
        final String username = "username";
        final String password = "password";
        User user = User.builder()
                .id(UUID.randomUUID().toString())
                .username(username)
                .password(passwordEncoder.encode(password))
                .build();
        userRepository.save(user);

        String token = given().contentType(ContentType.JSON)
                .body(new TokenController.UsernamePassword(username, password))
                .post("/tokens")
                .then()
                .statusCode(200)
                .extract()
                .body().path("token");

        User response = given().auth().oauth2(token)
                .get("/test/me")
                .then()
                .statusCode(200)
                .extract()
                .body().as(User.class);
        assertEquals(user, response);
    }
}
